﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DBG35FProject
{
    public partial class Update : Form
    {
        public Update()
        {
            InitializeComponent();
        }

        private void Update_Load(object sender, EventArgs e)
        {
            id.Items.Clear();

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select StudentID from Student ", con);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {

                id.Items.Add(dr["StudentID"].ToString());


            }
        }

        private void add_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Update Student set FirstName=@FirstName ,LastName=@LastName, RegistrationNo=@RegistrationNo ,Password=@Password ,DOB=@DOB, Contact=@Contact,Address=@Address, RoomNo=@RoomNo where StudentID=@Id", con);
            cmd.Parameters.AddWithValue("@Id", id.Text);
            cmd.Parameters.AddWithValue("@FirstName", fName.Text);
            cmd.Parameters.AddWithValue("@LastName", lName.Text);
            cmd.Parameters.AddWithValue("@RegistrationNo", reg_No.Text);
            cmd.Parameters.AddWithValue("@Password", pass.Text);
            cmd.Parameters.AddWithValue("@DOB", date.Value);
            cmd.Parameters.AddWithValue("@Contact", contact.Text);
            cmd.Parameters.AddWithValue("@Address", address.Text);
            cmd.Parameters.AddWithValue("@RoomNo", room.Text);
            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Student has been Updated Successfully!!", "Completed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                id.Text = "";
                fName.Text = "";
                lName.Text = "";
                reg_No.Text = "";
                pass.Text = "";
                date.Text = "";
                contact.Text = "";
                address.Text = "";
                room.Text = "";
            }
            catch (Exception)
            {
                MessageBox.Show("Erron on Updating Staff", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * From Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int i = e.RowIndex;
            id.Text = dataGridView1.Rows[i].Cells[0].Value.ToString();
            fName.Text = dataGridView1.Rows[i].Cells[1].Value.ToString();
            lName.Text = dataGridView1.Rows[i].Cells[2].Value.ToString();
            reg_No.Text = dataGridView1.Rows[i].Cells[3].Value.ToString();
            pass.Text = dataGridView1.Rows[i].Cells[4].Value.ToString();
            date.Text = dataGridView1.Rows[i].Cells[5].Value.ToString();
            contact.Text = dataGridView1.Rows[i].Cells[6].Value.ToString();
            address.Text = dataGridView1.Rows[i].Cells[7].Value.ToString();
            room.Text = dataGridView1.Rows[i].Cells[8].Value.ToString();           
        }
    }
}
