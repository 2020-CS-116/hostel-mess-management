﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DBG35FProject
{
    public partial class StdForm : Form
    {
        public StdForm()
        {
            InitializeComponent();
        }

        private void StdForm_Load(object sender, EventArgs e)
        {
           
        }

        private void add_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "" || textBox5.Text == "" || textBox6.Text == "" || textBox7.Text == "" || dateTimePicker1.Text == "")
            {
                MessageBox.Show("Please Enter the Details of Student First!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            else
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Student values (@FirstName ,@LastName, @RegistrationNo ,@Password ,@DOB, @Contact, @Address, @RoomNo)", con);
                cmd.Parameters.AddWithValue("@FirstName", textBox1.Text);
                cmd.Parameters.AddWithValue("@LastName", textBox2.Text);
                cmd.Parameters.AddWithValue("@RegistrationNo", textBox3.Text);
                cmd.Parameters.AddWithValue("@Password", textBox7.Text);
                cmd.Parameters.AddWithValue("@DOB", DateTime.Parse(dateTimePicker1.Text));
                cmd.Parameters.AddWithValue("@Contact", textBox4.Text);
                cmd.Parameters.AddWithValue("@Address", textBox5.Text);
                cmd.Parameters.AddWithValue("@RoomNo", int.Parse(textBox6.Text));

                cmd.ExecuteNonQuery();
                MessageBox.Show("Student has been added Successfully!!", "Completed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                textBox1.Text = "";
                textBox2.Text = "";
                textBox3.Text = "";
                textBox4.Text = "";
                textBox5.Text = "";
                textBox6.Text = "";
                textBox7.Text = "";
                dateTimePicker1.Text = "";
            }

                                       
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * From Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    }
}
