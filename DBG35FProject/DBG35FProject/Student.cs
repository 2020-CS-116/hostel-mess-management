﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBG35FProject
{
    public partial class Student : Form
    {
        public string Id { get; set; }
        public Student(string id)
        {
            Id = id;
            InitializeComponent();
        }

        private void Student_Load(object sender, EventArgs e)
        {

        }

        private void exit_Click(object sender, EventArgs e)
        {
            DialogResult dia;
            dia = MessageBox.Show("Do You want to Exit", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dia == DialogResult.Yes)
            {
                this.Hide();
                DashBoard db = new DashBoard();
                db.Show();
            }
            else
            {
                this.Show();
            }
        }

        private void attendence_Click(object sender, EventArgs e)
        {
            this.panel1.Controls.Clear();
            S_Bill atd = new S_Bill(Id) { Dock = DockStyle.Fill, TopLevel = false, TopMost = true };
            this.panel1.Controls.Add(atd);
            atd.Show();
        }

        private void guna2CircleButton1_Click(object sender, EventArgs e)
        {
            this.panel1.Controls.Clear();
            S_Attendence atd = new S_Attendence(Id) { Dock = DockStyle.Fill, TopLevel = false, TopMost = true };
            this.panel1.Controls.Add(atd);
            atd.Show();
        }

        private void guna2CircleButton2_Click(object sender, EventArgs e)
        {
            this.panel1.Controls.Clear();
            S_Food atd = new S_Food() { Dock = DockStyle.Fill, TopLevel = false, TopMost = true };
            this.panel1.Controls.Add(atd);
            atd.Show();
        }
    }
}
