﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DBG35FProject
{
    public partial class St_Login : Form
    {
        public St_Login()
        {
            InitializeComponent();
        }

        private void St_Login_Load(object sender, EventArgs e)
        {
            panel1.BackColor = Color.FromArgb(100, 0, 0, 0);
        }

        private void enter_Click(object sender, EventArgs e)
        {
            if (username.Text == "" || password.Text == "")
            {
                MessageBox.Show("Enter login Details First", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                var con = Configuration.getInstance().getConnection();
                String id = username.Text;
                String pass = password.Text.ToString();

                //Query to check Login Details
                String query = "Select ID , Password From Staff where ID = '" + username.Text + "' AND Password = '" + password.Text + "'  ";

                SqlDataAdapter da = new SqlDataAdapter(query, con);
                DataTable dt = new DataTable();
                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    id = username.Text;
                    pass = password.Text;

                    //Code to Show the next Form
                    this.Hide();
                    Staff st = new Staff();
                    st.Show();

                }
                else
                {
                    MessageBox.Show("Invalid Login", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void exit_Click(object sender, EventArgs e)
        {
            DialogResult dia;
            dia = MessageBox.Show("Do You want to Exit", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dia == DialogResult.Yes)
            {
                this.Hide();
                DashBoard db = new DashBoard();
                db.Show();
            }
            else
            {
                this.Show();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (password.PasswordChar == '*')
            {
                button1.BringToFront();
                password.PasswordChar = '\0';
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (password.PasswordChar == '\0')
            {
                button2.BringToFront();
                password.PasswordChar = '*';
            }
        }
    }
}