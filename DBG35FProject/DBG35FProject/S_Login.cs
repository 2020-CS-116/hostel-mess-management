﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DBG35FProject
{
    public partial class S_Login : Form
    {
        public S_Login()
        {
            InitializeComponent();
        }

        private void S_Login_Load(object sender, EventArgs e)
        {
            panel1.BackColor = Color.FromArgb(100, 0, 0, 0);
        }

        private void guna2CircleButton2_Click(object sender, EventArgs e)
        {
            DialogResult dia;
            dia = MessageBox.Show("Do You want to Exit", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dia == DialogResult.Yes)
            {
                this.Hide();
                DashBoard db = new DashBoard();
                db.Show();
            }
            else
            {
                this.Show();
            }
        }

        private void guna2CircleButton1_Click(object sender, EventArgs e)
        {
            if (username.Text == "" || textBox1.Text == "")
            {
                MessageBox.Show("Enter login Details First", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                var con = Configuration.getInstance().getConnection();
                String id = username.Text;
                String pass = textBox1.Text.ToString();

                //Query to check Login Details
                String query = "Select StudentID , Password From Student where StudentID = '" + username.Text + "' AND Password = '" + textBox1.Text + "'  ";

                SqlDataAdapter da = new SqlDataAdapter(query, con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                
                if (dt.Rows.Count > 0)
                {
                    id = username.Text;
                    pass = textBox1.Text;

                    
                    //Code to Show the next Form
                    this.Hide();
                    Student st = new Student(id);
                    st.Show();

                }
                else
                {
                    MessageBox.Show("Invalid Login", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }   

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.PasswordChar == '*')
            {
                button1.BringToFront();
                textBox1.PasswordChar = '\0';
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.PasswordChar == '\0')
            {
                button2.BringToFront();
                textBox1.PasswordChar = '*';
            }
        }
    }
}
