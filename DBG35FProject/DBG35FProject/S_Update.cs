﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DBG35FProject
{
    public partial class S_Update : Form
    {
        public S_Update()
        {
            InitializeComponent();
        }

        private void add_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Update Staff set FirstName=@FirstName ,LastName=@LastName, Designation=@Designation ,Password=@Password ,DOB=@DOB, Contact=@Contact,Address=@Address where ID=@Id", con);
            cmd.Parameters.AddWithValue("@Id", id.Text);
            cmd.Parameters.AddWithValue("@FirstName", fName.Text);
            cmd.Parameters.AddWithValue("@LastName", lName.Text);
            cmd.Parameters.AddWithValue("@Designation", desg.Text);
            cmd.Parameters.AddWithValue("@Password", pass.Text);
            cmd.Parameters.AddWithValue("@DOB", date.Value);
            cmd.Parameters.AddWithValue("@Contact", cont.Text);
            cmd.Parameters.AddWithValue("@Address", address.Text);
            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Staff Details has been updated Successfully", "Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                id.Text = " ";
                fName.Text = " ";
                lName.Text = " ";
                desg.Text = " ";
                cont.Text = " ";
                address.Text = " ";
                pass.Text = " ";
            }
            catch (Exception)
            {
                MessageBox.Show("Erron on Updating Staff", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * From Staff", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void S_Update_Load(object sender, EventArgs e)
        {
            id.Items.Clear();

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select ID from Staff ", con);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {

                id.Items.Add(dr["Id"].ToString());


            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int i = e.RowIndex;
            id.Text = dataGridView1.Rows[i].Cells[0].Value.ToString();
            fName.Text = dataGridView1.Rows[i].Cells[1].Value.ToString();
            lName.Text = dataGridView1.Rows[i].Cells[2].Value.ToString();
            desg.Text = dataGridView1.Rows[i].Cells[3].Value.ToString();
            pass.Text = dataGridView1.Rows[i].Cells[4].Value.ToString();
            date.Text = dataGridView1.Rows[i].Cells[5].Value.ToString();
            cont.Text = dataGridView1.Rows[i].Cells[6].Value.ToString();
            address.Text = dataGridView1.Rows[i].Cells[7].Value.ToString();
        }
    }
}
