﻿
namespace DBG35FProject
{
    partial class Staff
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.DropDown = new System.Windows.Forms.FlowLayoutPanel();
            this.attendence = new Guna.UI2.WinForms.Guna2CircleButton();
            this.mess = new Guna.UI2.WinForms.Guna2CircleButton();
            this.bill = new Guna.UI2.WinForms.Guna2CircleButton();
            this.record = new Guna.UI2.WinForms.Guna2CircleButton();
            this.exit = new Guna.UI2.WinForms.Guna2CircleButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.main_Panel = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.guna2CircleButton1 = new Guna.UI2.WinForms.Guna2CircleButton();
            this.DropDown.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(290, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(431, 39);
            this.label1.TabIndex = 1;
            this.label1.Text = "Welcome to Staff Section";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // DropDown
            // 
            this.DropDown.Controls.Add(this.attendence);
            this.DropDown.Controls.Add(this.mess);
            this.DropDown.Controls.Add(this.bill);
            this.DropDown.Controls.Add(this.record);
            this.DropDown.Controls.Add(this.guna2CircleButton1);
            this.DropDown.Controls.Add(this.exit);
            this.DropDown.Location = new System.Drawing.Point(2, 94);
            this.DropDown.Name = "DropDown";
            this.DropDown.Size = new System.Drawing.Size(155, 586);
            this.DropDown.TabIndex = 2;
            // 
            // attendence
            // 
            this.attendence.BackColor = System.Drawing.Color.Transparent;
            this.attendence.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.attendence.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.attendence.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.attendence.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.attendence.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.attendence.FocusedColor = System.Drawing.Color.White;
            this.attendence.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.attendence.ForeColor = System.Drawing.Color.White;
            this.attendence.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.attendence.HoverState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.attendence.HoverState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.attendence.Location = new System.Drawing.Point(3, 3);
            this.attendence.Name = "attendence";
            this.attendence.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.attendence.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(10);
            this.attendence.Size = new System.Drawing.Size(152, 69);
            this.attendence.TabIndex = 19;
            this.attendence.Text = "Mark Attendence";
            this.attendence.Click += new System.EventHandler(this.attendence_Click);
            // 
            // mess
            // 
            this.mess.BackColor = System.Drawing.Color.Transparent;
            this.mess.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.mess.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.mess.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.mess.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.mess.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.mess.FocusedColor = System.Drawing.Color.White;
            this.mess.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mess.ForeColor = System.Drawing.Color.White;
            this.mess.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.mess.HoverState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.mess.HoverState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.mess.Location = new System.Drawing.Point(3, 78);
            this.mess.Name = "mess";
            this.mess.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.mess.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(10);
            this.mess.Size = new System.Drawing.Size(152, 69);
            this.mess.TabIndex = 22;
            this.mess.Text = "Mess Food";
            this.mess.Click += new System.EventHandler(this.mess_Click);
            // 
            // bill
            // 
            this.bill.BackColor = System.Drawing.Color.Transparent;
            this.bill.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.bill.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.bill.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.bill.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.bill.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.bill.FocusedColor = System.Drawing.Color.White;
            this.bill.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bill.ForeColor = System.Drawing.Color.White;
            this.bill.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.bill.HoverState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bill.HoverState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.bill.Location = new System.Drawing.Point(3, 153);
            this.bill.Name = "bill";
            this.bill.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.bill.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(10);
            this.bill.Size = new System.Drawing.Size(152, 69);
            this.bill.TabIndex = 17;
            this.bill.Text = "Generate Bill";
            this.bill.Click += new System.EventHandler(this.bill_Click);
            // 
            // record
            // 
            this.record.BackColor = System.Drawing.Color.Transparent;
            this.record.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.record.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.record.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.record.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.record.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.record.FocusedColor = System.Drawing.Color.White;
            this.record.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.record.ForeColor = System.Drawing.Color.White;
            this.record.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.record.HoverState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.record.HoverState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.record.Location = new System.Drawing.Point(3, 228);
            this.record.Name = "record";
            this.record.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.record.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(10);
            this.record.Size = new System.Drawing.Size(152, 69);
            this.record.TabIndex = 21;
            this.record.Text = "Mess Record";
            this.record.Click += new System.EventHandler(this.record_Click);
            // 
            // exit
            // 
            this.exit.BackColor = System.Drawing.Color.Transparent;
            this.exit.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.exit.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.exit.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.exit.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.exit.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.exit.FocusedColor = System.Drawing.Color.White;
            this.exit.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exit.ForeColor = System.Drawing.Color.White;
            this.exit.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.exit.HoverState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.exit.HoverState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.exit.Location = new System.Drawing.Point(3, 378);
            this.exit.Name = "exit";
            this.exit.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.exit.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(10);
            this.exit.Size = new System.Drawing.Size(152, 69);
            this.exit.TabIndex = 20;
            this.exit.Text = "Exit";
            this.exit.Click += new System.EventHandler(this.exit_Click_1);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::DBG35FProject.Properties.Resources.staff;
            this.pictureBox1.Location = new System.Drawing.Point(2, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(155, 94);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // main_Panel
            // 
            this.main_Panel.Location = new System.Drawing.Point(163, 94);
            this.main_Panel.Name = "main_Panel";
            this.main_Panel.Size = new System.Drawing.Size(817, 586);
            this.main_Panel.TabIndex = 3;
            // 
            // guna2CircleButton1
            // 
            this.guna2CircleButton1.BackColor = System.Drawing.Color.Transparent;
            this.guna2CircleButton1.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2CircleButton1.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2CircleButton1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2CircleButton1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2CircleButton1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.guna2CircleButton1.FocusedColor = System.Drawing.Color.White;
            this.guna2CircleButton1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2CircleButton1.ForeColor = System.Drawing.Color.White;
            this.guna2CircleButton1.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.guna2CircleButton1.HoverState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.guna2CircleButton1.HoverState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.guna2CircleButton1.Location = new System.Drawing.Point(3, 303);
            this.guna2CircleButton1.Name = "guna2CircleButton1";
            this.guna2CircleButton1.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.guna2CircleButton1.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(10);
            this.guna2CircleButton1.Size = new System.Drawing.Size(152, 69);
            this.guna2CircleButton1.TabIndex = 23;
            this.guna2CircleButton1.Text = "Reports";
            this.guna2CircleButton1.Click += new System.EventHandler(this.guna2CircleButton1_Click);
            // 
            // Staff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::DBG35FProject.Properties.Resources._21;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(992, 692);
            this.Controls.Add(this.main_Panel);
            this.Controls.Add(this.DropDown);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Staff";
            this.Text = "Staff";
            this.Load += new System.EventHandler(this.Staff_Load);
            this.DropDown.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel DropDown;
        private Guna.UI2.WinForms.Guna2GradientPanel main_Panel;
        private Guna.UI2.WinForms.Guna2CircleButton bill;
        private Guna.UI2.WinForms.Guna2CircleButton exit;
        private Guna.UI2.WinForms.Guna2CircleButton attendence;
        private Guna.UI2.WinForms.Guna2CircleButton record;
        private Guna.UI2.WinForms.Guna2CircleButton mess;
        private Guna.UI2.WinForms.Guna2CircleButton guna2CircleButton1;
    }
}