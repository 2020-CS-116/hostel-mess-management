﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DBG35FProject
{
    public partial class Search : Form
    {
        public Search()
        {
            InitializeComponent();
        }

        private void Search_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select StudentID from Student ", con);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {

                comboBox1.Items.Add(dr["StudentID"].ToString());


            }
            panel1.BackColor = Color.FromArgb(100, 0, 0, 0);
        }


        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("exec stpGetAllProducts", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "")
            {
                MessageBox.Show("Enter the Id you want to Search!!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select StudentID, FirstName , LastName , RegistrationNo, Password, DOB, Contact, Address, RoomNo" +
                "                             From Student" +
                "                             Where StudentID = @Id", con);
                cmd.Parameters.AddWithValue("@Id", comboBox1.Text);

                //Data Will Shown in Table
                SqlDataAdapter daa = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                daa.Fill(dt);
                dataGridView1.DataSource = dt;

                //Data will be Shown in text Boxes
                SqlDataReader da = cmd.ExecuteReader();
                while (da.Read())
                {
                    std.Text = da.GetValue(0).ToString();
                    first.Text = da.GetValue(1).ToString();
                    last.Text = da.GetValue(2).ToString();
                    no.Text = da.GetValue(3).ToString();
                    password.Text = da.GetValue(4).ToString();
                    birth.Text = da.GetValue(5).ToString();
                    cont.Text = da.GetValue(6).ToString();
                    add.Text = da.GetValue(7).ToString();
                    room.Text = da.GetValue(8).ToString();
                }
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            int i = e.RowIndex;
            std.Text = dataGridView1.Rows[i].Cells[0].Value.ToString();
            first.Text = dataGridView1.Rows[i].Cells[1].Value.ToString();
            last.Text = dataGridView1.Rows[i].Cells[2].Value.ToString();
            no.Text = dataGridView1.Rows[i].Cells[3].Value.ToString();
            password.Text = dataGridView1.Rows[i].Cells[4].Value.ToString();
            birth.Text = dataGridView1.Rows[i].Cells[5].Value.ToString();
            cont.Text = dataGridView1.Rows[i].Cells[6].Value.ToString();
            add.Text = dataGridView1.Rows[i].Cells[7].Value.ToString();
            room.Text = dataGridView1.Rows[i].Cells[8].Value.ToString();

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

    }
}
