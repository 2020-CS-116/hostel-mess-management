USE [newFinal]
GO
/****** Object:  Table [dbo].[Student]    Script Date: 30/04/2022 5:37:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student](
	[StudentID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](30) NOT NULL,
	[LastName] [nvarchar](30) NULL,
	[RegistrationNo] [nvarchar](25) NOT NULL,
	[Password] [nvarchar](25) NOT NULL,
	[DOB] [datetime] NULL,
	[Contact] [nvarchar](40) NULL,
	[Address] [nvarchar](100) NULL,
	[RoomNo] [int] NOT NULL,
 CONSTRAINT [PK_Student] PRIMARY KEY CLUSTERED 
(
	[StudentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[StudentLogin]    Script Date: 30/04/2022 5:37:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[StudentLogin]
AS
SELECT        StudentID, RegistrationNo, Password
FROM            dbo.Student
GO
/****** Object:  Table [dbo].[Staff]    Script Date: 30/04/2022 5:37:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Staff](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](30) NOT NULL,
	[LastName] [nvarchar](30) NULL,
	[Designation] [nvarchar](30) NOT NULL,
	[Password] [nvarchar](25) NOT NULL,
	[DOB] [datetime] NULL,
	[Contact] [nvarchar](40) NULL,
	[Address] [nvarchar](100) NULL,
 CONSTRAINT [PK_Staff] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[StaffLogin]    Script Date: 30/04/2022 5:37:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[StaffLogin]
AS
SELECT        ID, Password, Designation
FROM            dbo.Staff
GO
/****** Object:  Table [dbo].[Food]    Script Date: 30/04/2022 5:37:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Food](
	[FoodID] [int] IDENTITY(1,1) NOT NULL,
	[DishName] [nvarchar](40) NOT NULL,
	[CostPerDish] [float] NOT NULL,
	[TotalDishes] [int] NOT NULL,
	[Day] [nvarchar](40) NOT NULL,
	[Shift] [nvarchar](40) NOT NULL,
	[SalePerDish] [float] NOT NULL,
 CONSTRAINT [PK_Food] PRIMARY KEY CLUSTERED 
(
	[FoodID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[Mess]    Script Date: 30/04/2022 5:37:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Mess]
AS
SELECT        FoodID, DishName, Shift, Day
FROM            dbo.Food
GO
/****** Object:  Table [dbo].[Bill]    Script Date: 30/04/2022 5:37:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bill](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StudentID] [int] NOT NULL,
	[AttendanceID] [int] NOT NULL,
	[Bill] [float] NOT NULL,
	[Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Bill] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[BillV]    Script Date: 30/04/2022 5:37:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[BillV]
AS
SELECT        ID, StudentID, Bill, Date
FROM            dbo.Bill
GO
/****** Object:  Table [dbo].[Attendance]    Script Date: 30/04/2022 5:37:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Attendance](
	[AttendanceID] [int] IDENTITY(1,1) NOT NULL,
	[StudentID] [int] NOT NULL,
	[StaffID] [int] NOT NULL,
	[FoodID] [int] NOT NULL,
	[Attendence] [int] NOT NULL,
	[Guest] [int] NULL,
	[Total_Attendence] [int] NOT NULL,
	[Total_Price] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Attendance] PRIMARY KEY CLUSTERED 
(
	[AttendanceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Record]    Script Date: 30/04/2022 5:37:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Record](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FoodId] [int] NULL,
	[Profit] [int] NOT NULL,
	[Total_Dishes] [int] NULL,
	[Sale_Dishes] [int] NULL,
	[Date] [datetime] NULL,
 CONSTRAINT [PK_Record] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Attendance] ON 

INSERT [dbo].[Attendance] ([AttendanceID], [StudentID], [StaffID], [FoodID], [Attendence], [Guest], [Total_Attendence], [Total_Price], [Date]) VALUES (1, 2, 3, 2, 1, 2, 3, 240, CAST(N'2022-04-30T00:00:00.000' AS DateTime))
INSERT [dbo].[Attendance] ([AttendanceID], [StudentID], [StaffID], [FoodID], [Attendence], [Guest], [Total_Attendence], [Total_Price], [Date]) VALUES (2, 4, 3, 2, 1, 0, 1, 80, CAST(N'2022-04-30T00:00:00.000' AS DateTime))
INSERT [dbo].[Attendance] ([AttendanceID], [StudentID], [StaffID], [FoodID], [Attendence], [Guest], [Total_Attendence], [Total_Price], [Date]) VALUES (3, 4, 4, 1, 1, 5, 6, 660, CAST(N'2022-04-30T00:00:00.000' AS DateTime))
INSERT [dbo].[Attendance] ([AttendanceID], [StudentID], [StaffID], [FoodID], [Attendence], [Guest], [Total_Attendence], [Total_Price], [Date]) VALUES (4, 6, 3, 2, 1, 0, 1, 80, CAST(N'2022-04-30T00:00:00.000' AS DateTime))
INSERT [dbo].[Attendance] ([AttendanceID], [StudentID], [StaffID], [FoodID], [Attendence], [Guest], [Total_Attendence], [Total_Price], [Date]) VALUES (5, 2, 3, 7, 1, 3, 4, 440, CAST(N'2022-04-30T00:00:00.000' AS DateTime))
INSERT [dbo].[Attendance] ([AttendanceID], [StudentID], [StaffID], [FoodID], [Attendence], [Guest], [Total_Attendence], [Total_Price], [Date]) VALUES (6, 3, 3, 5, 0, 0, 0, 0, CAST(N'2022-04-30T00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[Attendance] OFF
GO
SET IDENTITY_INSERT [dbo].[Bill] ON 

INSERT [dbo].[Bill] ([ID], [StudentID], [AttendanceID], [Bill], [Date]) VALUES (1, 2, 7, 680, CAST(N'2022-04-30T00:00:00.000' AS DateTime))
INSERT [dbo].[Bill] ([ID], [StudentID], [AttendanceID], [Bill], [Date]) VALUES (2, 3, 0, 0, CAST(N'2022-04-30T00:00:00.000' AS DateTime))
INSERT [dbo].[Bill] ([ID], [StudentID], [AttendanceID], [Bill], [Date]) VALUES (3, 4, 7, 740, CAST(N'2022-04-30T00:00:00.000' AS DateTime))
INSERT [dbo].[Bill] ([ID], [StudentID], [AttendanceID], [Bill], [Date]) VALUES (4, 6, 1, 80, CAST(N'2022-04-30T00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[Bill] OFF
GO
SET IDENTITY_INSERT [dbo].[Food] ON 

INSERT [dbo].[Food] ([FoodID], [DishName], [CostPerDish], [TotalDishes], [Day], [Shift], [SalePerDish]) VALUES (1, N'Chicken Karahi', 110, 150, N'Monday', N'Day', 130)
INSERT [dbo].[Food] ([FoodID], [DishName], [CostPerDish], [TotalDishes], [Day], [Shift], [SalePerDish]) VALUES (2, N'Daal Chawal', 80, 150, N'Monday', N'Night', 110)
INSERT [dbo].[Food] ([FoodID], [DishName], [CostPerDish], [TotalDishes], [Day], [Shift], [SalePerDish]) VALUES (3, N'Roast', 90, 200, N'Tuesday', N'Day', 110)
INSERT [dbo].[Food] ([FoodID], [DishName], [CostPerDish], [TotalDishes], [Day], [Shift], [SalePerDish]) VALUES (4, N'Sabzi', 60, 100, N'Tuesday', N'Night', 80)
INSERT [dbo].[Food] ([FoodID], [DishName], [CostPerDish], [TotalDishes], [Day], [Shift], [SalePerDish]) VALUES (5, N'Biryani', 110, 250, N'Wednesday', N'Day', 150)
INSERT [dbo].[Food] ([FoodID], [DishName], [CostPerDish], [TotalDishes], [Day], [Shift], [SalePerDish]) VALUES (6, N'Haleem', 90, 150, N'Wednesday', N'Night', 110)
INSERT [dbo].[Food] ([FoodID], [DishName], [CostPerDish], [TotalDishes], [Day], [Shift], [SalePerDish]) VALUES (7, N'Aaloo Keema ', 110, 120, N'Thursday', N'Day', 130)
INSERT [dbo].[Food] ([FoodID], [DishName], [CostPerDish], [TotalDishes], [Day], [Shift], [SalePerDish]) VALUES (8, N'Surprise', 110, 250, N'Thursday', N'Night', 150)
INSERT [dbo].[Food] ([FoodID], [DishName], [CostPerDish], [TotalDishes], [Day], [Shift], [SalePerDish]) VALUES (9, N'Biryani', 110, 250, N'Friday', N'Day', 150)
SET IDENTITY_INSERT [dbo].[Food] OFF
GO
SET IDENTITY_INSERT [dbo].[Record] ON 

INSERT [dbo].[Record] ([ID], [FoodId], [Profit], [Total_Dishes], [Sale_Dishes], [Date]) VALUES (1, 1, 2200, 150, 130, CAST(N'2022-04-30T00:00:00.000' AS DateTime))
INSERT [dbo].[Record] ([ID], [FoodId], [Profit], [Total_Dishes], [Sale_Dishes], [Date]) VALUES (2, 3, 5400, 200, 140, CAST(N'2022-04-30T00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[Record] OFF
GO
SET IDENTITY_INSERT [dbo].[Staff] ON 

INSERT [dbo].[Staff] ([ID], [FirstName], [LastName], [Designation], [Password], [DOB], [Contact], [Address]) VALUES (3, N'Ali', N'Mehar', N'in-Charge', N'123', CAST(N'2022-04-21T22:49:42.000' AS DateTime), N'0300875656', N'daska')
INSERT [dbo].[Staff] ([ID], [FirstName], [LastName], [Designation], [Password], [DOB], [Contact], [Address]) VALUES (4, N'Asad', N'umer', N'In-Charge', N'1234', CAST(N'2022-04-30T00:00:00.000' AS DateTime), N'03131312311', N'Lahore')
INSERT [dbo].[Staff] ([ID], [FirstName], [LastName], [Designation], [Password], [DOB], [Contact], [Address]) VALUES (5, N' Ahmed', N'Mehar', N'Cook', N' 123', CAST(N'2022-04-30T00:00:00.000' AS DateTime), N'03918231123', N' Lahore')
INSERT [dbo].[Staff] ([ID], [FirstName], [LastName], [Designation], [Password], [DOB], [Contact], [Address]) VALUES (6, N' Umer', N' Abid', N' Waiter', N' 123', CAST(N'2022-04-30T00:00:00.000' AS DateTime), N' 0345454512', N' Lahore')
SET IDENTITY_INSERT [dbo].[Staff] OFF
GO
SET IDENTITY_INSERT [dbo].[Student] ON 

INSERT [dbo].[Student] ([StudentID], [FirstName], [LastName], [RegistrationNo], [Password], [DOB], [Contact], [Address], [RoomNo]) VALUES (2, N'Muhammad', N'Umar', N'2020-CS-116', N'123', CAST(N'2022-04-27T00:00:00.000' AS DateTime), N'03454565554', N'Lahore', 12)
INSERT [dbo].[Student] ([StudentID], [FirstName], [LastName], [RegistrationNo], [Password], [DOB], [Contact], [Address], [RoomNo]) VALUES (3, N'Asad', N'Bhatti', N'2020-CS-101', N'hello', CAST(N'2022-04-30T00:00:00.000' AS DateTime), N'03214321454', N'HafzaBad', 57)
INSERT [dbo].[Student] ([StudentID], [FirstName], [LastName], [RegistrationNo], [Password], [DOB], [Contact], [Address], [RoomNo]) VALUES (4, N'Qamar', N'Mehar', N'2020-cs-155', N'123', CAST(N'2022-04-30T00:00:00.000' AS DateTime), N'03421212131', N'Daska', 172)
INSERT [dbo].[Student] ([StudentID], [FirstName], [LastName], [RegistrationNo], [Password], [DOB], [Contact], [Address], [RoomNo]) VALUES (5, N'Umer', N'Amjad', N'2020-cs-61', N'123', CAST(N'2022-04-30T00:00:00.000' AS DateTime), N'03123123112', N'Sahiwal', 142)
INSERT [dbo].[Student] ([StudentID], [FirstName], [LastName], [RegistrationNo], [Password], [DOB], [Contact], [Address], [RoomNo]) VALUES (6, N'Salahuddin', N'chahhal', N'2020-cs-85', N'12345', CAST(N'2022-04-30T00:00:00.000' AS DateTime), N'03123123123', N'Gujranwala', 172)
SET IDENTITY_INSERT [dbo].[Student] OFF
GO
ALTER TABLE [dbo].[Attendance]  WITH CHECK ADD  CONSTRAINT [FK_Attendance_Food] FOREIGN KEY([FoodID])
REFERENCES [dbo].[Food] ([FoodID])
GO
ALTER TABLE [dbo].[Attendance] CHECK CONSTRAINT [FK_Attendance_Food]
GO
ALTER TABLE [dbo].[Attendance]  WITH CHECK ADD  CONSTRAINT [FK_Attendance_Staff] FOREIGN KEY([StaffID])
REFERENCES [dbo].[Staff] ([ID])
GO
ALTER TABLE [dbo].[Attendance] CHECK CONSTRAINT [FK_Attendance_Staff]
GO
ALTER TABLE [dbo].[Attendance]  WITH CHECK ADD  CONSTRAINT [FK_Attendance_Student] FOREIGN KEY([StudentID])
REFERENCES [dbo].[Student] ([StudentID])
GO
ALTER TABLE [dbo].[Attendance] CHECK CONSTRAINT [FK_Attendance_Student]
GO
ALTER TABLE [dbo].[Bill]  WITH CHECK ADD  CONSTRAINT [FK_Bill_Student] FOREIGN KEY([StudentID])
REFERENCES [dbo].[Student] ([StudentID])
GO
ALTER TABLE [dbo].[Bill] CHECK CONSTRAINT [FK_Bill_Student]
GO
ALTER TABLE [dbo].[Record]  WITH CHECK ADD  CONSTRAINT [FK_Record_Food] FOREIGN KEY([FoodId])
REFERENCES [dbo].[Food] ([FoodID])
GO
ALTER TABLE [dbo].[Record] CHECK CONSTRAINT [FK_Record_Food]
GO
/****** Object:  StoredProcedure [dbo].[stpGetAllAttendance]    Script Date: 30/04/2022 5:37:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[stpGetAllAttendance]

AS
BEGIN
	SET NOCOUNT ON;

    
	SELECT * From Attendance;
END
GO
/****** Object:  StoredProcedure [dbo].[stpGetAllFood]    Script Date: 30/04/2022 5:37:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[stpGetAllFood]

AS
BEGIN
	SET NOCOUNT ON;

    
	SELECT * From Mess;
END
GO
/****** Object:  StoredProcedure [dbo].[stpGetAllProducts]    Script Date: 30/04/2022 5:37:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[stpGetAllProducts] 

AS
BEGIN
	SET NOCOUNT ON;

    
	SELECT * From Student;
END
GO
/****** Object:  StoredProcedure [dbo].[stpGetAllStaff]    Script Date: 30/04/2022 5:37:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[stpGetAllStaff]

AS
BEGIN
	SET NOCOUNT ON;

    
	SELECT * From Staff;
END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Bill"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BillV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BillV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Food"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 2
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Mess'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Mess'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Staff"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 2
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'StaffLogin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'StaffLogin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Student"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 5
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'StudentLogin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'StudentLogin'
GO
